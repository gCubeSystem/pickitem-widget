
# Changelog for Pick Item widget

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.0.0-SNAPSHOT] - 2022-06-15

- Ported to git
- Ported to GWT 2.9

## [v1.3.0] - 2018-03-19

- Enhanced information passed when an item is selected, now passing more info such as if the item is a group or not and the item id 

## [v1.2.0] - 2016-06-30

- Fix for Incident #4246, in some cases notification email resulting from posts are "broken"
- Fix for hashtags not allowing to use enter after picked one

## [v1.1.0] - 2015-07-14

- Ported to GWT 2.7.0
- Fix for Bug #320, Users tagging does not work if @ is in the middle of already typed text

## [v1.0.0] - 2013-10-21

- First release
